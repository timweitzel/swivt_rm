%CHP Plant sollte mindestens 5000 h im Jahr laufen und nicht k?rzer als 1h
%am St?ck laufen
par.CHP.c_fuel = 0.04; %4 ct / kWh HO
par.CHP.c_startup = 0; % [EUR] -- (1/3) einer Periode kann nur die Halbe Leistung verwendet werden (approximiert)
% ==> c_fuel * P_CHP * (1/3) * 2
par.CHP.alpha = [9999; 2.720; 2.107; 1.975];%[999999; 999999; 2.720; 2.107; 1.975]; % bei der Erzeugung von 1kW elektrische Leistung werden alpha kW thermische Leistung durch Abw??rme erzeugt
par.CHPmodes=[0; 0.50; 0.75; 1.0];%[0; 0.499999; 0.50; 0.75; 1.0];%2.720; 2.107; 1.975]; %Percent of rated electrical power
par.CHP.eta = [0.00001; 0.699; 0.613; 0.613];%;[0.0002; 0.0002; 0.699; 0.611; 0.613]; % W?rmewirkungsgrad
par.n_CHPmodes=length(par.CHPmodes);
% Laut Viessmann-Website sind typische Werte alpha = 1.5 ... 2
% entspricht REZIPROKE STROMKENNZAHL: 0.5
par.CHP.P = 20;
par.CHP.P_max = par.CHP.P;
par.CHP.P_min = par.CHP.P*0.5;% [kW] Betriebspunkt 50/81 als andere Variante

%par.CHP.etaP = 0.322; % elektrischer Wirkungsgradwirkungsgrad
%par.CHP.etaG= par.CHP.etaP+par.CHP.eta;
par.CHP.t_min = 1 + 3;%mindestlaufzeit an perioden. Mindestens 1 + x
%par.CHP.Curt_max=par.CHP.alpha(numel(par.CHP.alpha))*par.CHP.P*0.7;
%Variable entfernt


%Energy Storage
%Battery is a LiFePo4 battery
par.ESS.cell.C_batt=2.3; %2.3 Ah per cell
par.ESS.cell.U_n=3.3; %3.3V nominal voltage
par.ESS.cell.E=par.ESS.cell.C_batt * par.ESS.cell.U_n; %7.59W per Cell
par.ESS.cell.n=14000; % in Summe ca 53,13kWh bei 7000 Zellen

par.ESS.sigma=83.333e-6; % entspricht 0.2\% pro Tag, mittlerer Wert in Chen et. al (2009)
%sigma_bat = 0.000027778; %sigma_SD = 0.00067; % pro Stunde; entspricht 2% self-discharge / Monat
%sigma_bat = 0.000125 % entspricht 0.3\% pro Tag, max. Wert in  Chen et. al (2009)
par.ESS.RTE=0.95;
par.ESS.eta_c=sqrt(par.ESS.RTE);
par.ESS.eta_d=sqrt(par.ESS.RTE);

par.ESS.E=100;%par.ESS.cell.E*par.ESS.cell.n/1000;
par.ESS.E_max=par.ESS.E;
par.ESS.E_min=0;
par.ESS.P_c_max=par.ESS.E*3;
par.ESS.P_d_max=par.ESS.E*3;
par.ESS.P_c_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_d_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_min=0;
par.ESS.price           = 200;
par.ESS.CAC.temp        = 25;
par.ESS.CAC.C_verl      = 0.2;
par.ESS.CAC.max_year    = 20;
par.ESS.CYC.n_max       = 10000;
par.ESS.E_inv=par.ESS.price * par.ESS.E;

% Heater
par.HTR.c_fuel=0.055; % Preis ist leicht h?her, da in KWK Betrieb teilweise Kosten wieder zur?ckgeholt werden k?nnen
par.HTR.Q_dot_max_burner = 300;
par.HTR.eta= 0.92*0.89; % 0.84Brennwert- (und nicht Heizwert-) -basierter Wirkungsgrad

% Thermal Heat pump
par.THP.P_max       = 150; %3}50kw Pumpen max elektrische kW
par.THP.COP         = 0.99; %COP


% Thermischer Energiespeichers
par.TESS.sigma= 0.0062; % 0.62% pro Stunde Verlust -- W???rme soll zwei Tage vorhalten
par.TESS.eta_c= 1;
par.TESS.eta_d= 1;
par.TESS.Q_max= par.CHP.P*par.CHP.alpha(numel(par.CHP.alpha)) * 50 * 1.163 * 30/1000;%50l/kw thermisch installiert * 1,163 Wh/kgK * Delta T=30K
par.TESS.Qdot_max= 100000000000;

% Netzgrenzen
par.Grid.P_d_max=1000;
par.Grid.P_s_max=1000;

% Preisinformationen

par.Price.pmult=1; %multiplier for given market prices

par.Price.customer      = 250; %EUR/MWh
par.Price.customer_heat = 65; %EUR/MWh
par.Price.EEG_umlage    = 63.54; %EUR/MWh
par.Price.Grid_AP       = 55.00; %EUR/MWh
par.Price.Grid_GP       = 2.78; %EUR/MWh
par.Price.sonstige      = 16.36; %EUR/MWh
par.Price.Stromsteuer   = 29.12; %EUR/MWh
par.Price.Konzession    = 15.90; %EUR/MWh

par.Price.Grid_Sell=   80; %EUR/MWh

par.Price.total_addon_micro= par.Price.EEG_umlage + par.Price.sonstige + par.Price.Grid_GP;
par.Price.total_addon_grid = par.Price.total_addon_micro + par.Price.Grid_AP + par.Price.Konzession + par.Price.Stromsteuer;
par.Price.NC_delta=par.Price.total_addon_grid-par.Price.total_addon_micro;

%LCC calculations
par.DODdelta=0.1;
par.DODvector=[0; 0.05; 0.15; 0.25; 0.35; 0.45; 0.55; 0.65; 0.75; 0.85; 0.95; 1.00];%[0:par.DODdelta:1];%
par.AhDODdelta=0.1;
par.AhDODvector=[0; 10];%
par.SOCdelta=0.1;
par.SOCvector=[0; 0.15; 0.30; 0.45; 0.60; 0.75; 0.90; 1.00];%0.00 ; 0.05; 0.10; 0.20; 0.30; 0.40; 0.45; 0.55; 0.60; 0.70; 0.80; 0.90; 0.95; 1.00];

par.DODApprox=length(par.DODvector)-1;%number of linearizations for DOD and SOC
par.SOCApprox=length(par.SOCvector);%number of linearizations for DOD and SOC
par.AhDODApprox=length(par.AhDODvector);


% Anzahl der perioden
par.delta_T     =  60; %min
par.T_O_Master  =  2; %Optimierungszeitraum in Tagen
par.T_O         =  par.T_O_Master;
par.T           =  24/(par.delta_T/60)*par.T_O_Master; %Optimierungszeitraum in Zeiteinheiten delta_T
par.T_1         =  par.T + 1;
par.T_C         =  24/(par.delta_T/60);
par.T_total     =  365; %kompletter Betrachtungszeitraum in Tagen
par.periods=1;
par.sdate.datetime(1) = datetime(2015, 1, 1);


%LCC Parameter
par.ESS.LCC.cyc_DOD=zeros(par.DODApprox,1);
for dprox=1:par.DODApprox
    for ahprox=1:par.AhDODApprox
        par.ESS.LCC.cyc_DOD(dprox,ahprox)= BA_cyc_Sarasketa_v2(     par.DODvector(dprox+1),    par.AhDODvector(ahprox)*par.ESS.cell.C_batt,    par.ESS.CAC.C_verl);
    end
end
for sprox=1:par.SOCApprox
    par.ESS.LCC.cal_SOC(sprox)= BA_cal_Sarasketa_v1(     (par.SOCvector(sprox)),    par.ESS.CAC.temp , par.delta_T,par.ESS.CAC.C_verl ,par.ESS.CAC.max_year);
end
clear dprox sprox ahprox

