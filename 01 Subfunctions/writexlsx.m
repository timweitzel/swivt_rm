function [Soltable]=writexlsx(Sol,folder,pvmax)

switch nargin
    case 1
        n_l=length(Sol.pv);
    case 2
        folderold=pwd;
        cd(folder)
        n_l=length(Sol.pv);
    case 3
        folderold=pwd;
        cd(folder)
        n_l=length(Sol.pv);
end


pv_names=fieldnames(Sol.pv);
KPI_names=fieldnames(Sol.Sim(1).KPI);
CHP_names=fieldnames(Sol.Sim(1).par.CHP);
THP_names=fieldnames(Sol.Sim(1).par.THP);
TESS_names=fieldnames(Sol.Sim(1).par.TESS);
BESS_names=fieldnames(Sol.Sim(1).par.ESS);
HTR_names=fieldnames(Sol.Sim(1).par.HTR);
Price_names=fieldnames(Sol.Sim(1).par.Price);
opt_names=fieldnames(Sol.Sim(1).opt);

for j=1:n_l

    if Sol.Sim(j).ID<=pvmax
        %Sol2.ID(j,1)=Sol.Sim(j).ID;
        for i=1:numel(pv_names)
            Sol2.(sprintf(strcat('pv_',pv_names{i})))(j,1)=Sol.pv(Sol.Sim(j).ID).(sprintf(pv_names{i}));
        end
        for i=1:numel(KPI_names)
            Sol2.(sprintf(strcat(KPI_names{i})))(j,1)=Sol.Sim(j).KPI.(sprintf(KPI_names{i}));
        end
        for i=1:numel(CHP_names)
            n=numel(Sol.Sim(j).par.CHP.(sprintf(CHP_names{i})));
            if n>1
                for m=1:n
                    Sol2.(sprintf(strcat('Par_CHP_',CHP_names{i},'_',num2str(Sol.par.CHPmodes(m)*100))))(j,1)=Sol.Sim(j).par.CHP.(sprintf(CHP_names{i}))(m);
                end
            else
                Sol2.(sprintf(strcat('Par_CHP_',CHP_names{i})))(j,1)=Sol.Sim(j).par.CHP.(sprintf(CHP_names{i}));
            end
        end
        for i=1:numel(THP_names)
            Sol2.(sprintf(strcat('Par_THP_',THP_names{i})))(j,1)=Sol.Sim(j).par.THP.(sprintf(THP_names{i}));
        end
        for i=1:numel(TESS_names)
            Sol2.(sprintf(strcat('Par_TESS_',TESS_names{i})))(j,1)=Sol.Sim(j).par.TESS.(sprintf(TESS_names{i}));
        end
        for i=1:numel(BESS_names)
            Sol2.(sprintf(strcat('Par_BESS_',BESS_names{i})))(j,1)=Sol.Sim(j).par.ESS.(sprintf(BESS_names{i}));
        end
        for i=1:numel(HTR_names)
            Sol2.(sprintf(strcat('Par_HTR_',HTR_names{i})))(j,1)=Sol.Sim(j).par.HTR.(sprintf(HTR_names{i}));
        end
        for i=1:numel(Price_names)
            Sol2.(sprintf(strcat('Par_Price_',Price_names{i})))(j,1)=Sol.Sim(j).par.Price.(sprintf(Price_names{i}));
        end
        for i=1:numel(opt_names)
            Sol2.(sprintf(strcat('OPT_',opt_names{i})))(j,1)=Sol.Sim(j).opt.(sprintf(opt_names{i}));
        end
    end
end

Soltable=struct2table(Sol2);
filename = strcat('outputdata_',datestr(now,'yy-mm-dd_HH-MM-SS'),'.xlsx');
% Table=res.machine_status_table;
writetable(Soltable,filename,'Sheet',1,'Range',strcat('A', '1' ));

switch nargin
    case 1      
    case 2
        cd(folderold)
    case 3
        cd(folderold)
end

end