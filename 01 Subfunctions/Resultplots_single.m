function resultplots_single(res, KPI, par, I)
handles=findall(0,'type','figure');
handles.delete;
Nplots=5;
        %% Netzbezug und -Einspeisung 
        %figure('Position',[20 100 700 700])
    
        %subplot(Nplots,2,1);
        figure(1)
        hold all;
        grid on;
        stairs(I.pd_t);
        ylabel('in EUR/MWh');
        lgd=legend('Price in DAM');
        set(lgd, 'Location', 'Northwest', 'Orientation','horizontal');
        %plot(zeros(size(result_z_G_demand)), 'k'); % schwarze Null-Linie zeichnen
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
                
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/1_Netzbezug')
        
        %% Energy production
        hold off;
        figure(2)
        %subplot(Nplots,2,3);
        hold all;
        grid on;
        stairs(I.PV_t());
        stairs(res.P_Curt_PV_T);
        stairs(res.P_CHP_T);
        lgd2=legend('P^{PV}_{elec}', 'P^{PV Curtailment}_{elec}', 'P^{CHP}_{elec}');
        set(lgd2, 'Location', 'Northwest', 'Orientation','horizontal');
        ylabel('in kW');
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/2_Production')

        %% Plot f?r elektrischen Energiespeicher - Inhalt und Leistungen
        hold off;
        figure(4)
        %subplot(Nplots,2,7);
        hold all;
        grid on;
        yyaxis left
        ylabel('in kW');
        ylim([-par.ESS.P_d_max par.ESS.P_c_max]);
        stairs(res.P_ESScrg_T-res.P_ESSdcrg_T, 'b-');
        
        yyaxis right
        ylabel('in kWh');
        ylim([0 par.ESS.E_max]);
        plot(res.E_ESSstore_T_1,'r-');

        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        lgd4=legend('P^{ESS}_{elec}', 'E^{ESS}_{elec}');
                set(lgd4, 'Location', 'Northwest', 'Orientation','horizontal');

        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/4_ESS')        


        %% Grid
        hold off;
        %subplot(Nplots,2,5);
        figure(3)
        hold all;
        grid on;
        stairs(res.P_Grid_d_T-res.P_Grid_s_T);
        stairs(I.Pres_t);
        lgd3=legend('Grid','P^{residual}_{elec}' );
        set(lgd3, 'Location', 'Northwest', 'Orientation','horizontal');
        ylabel('in kW');
        
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/3_Grid')   
        
        
        %% Total Production
        hold off;
        %subplot(Nplots,2,9);
        figure(5)
        hold all;
        grid on

        prod=(res.P_Grid_d_T  + res.P_ESSdcrg_T + I.PV_t - res.P_Curt_PV_T + res.P_CHP_T)*par.delta_T/60;
        cons=I.PDemand_t*par.delta_T/60 + (res.P_Grid_s_T + res.P_ESScrg_T + res.P_THP_T)*par.delta_T/60;
        y=[prod';cons'];
        bar(y')

        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        lgd5=legend('Production^{total}_{elec}', 'Consumption^{total}_{elec}');
        set(lgd5, 'Location', 'Northwest', 'Orientation','horizontal');
        xlabel('time');
        ylabel('in kWh')

        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/5_Totale')   
        
        
        %% W?rme CHP bin
        hold off;
        figure(6)
        %subplot(Nplots,2,4);
        hold all;
        grid on
        stem(res.S_run_CHP_T, '-');  
        stem(2*res.S_sup_CHP_T, '-');
        ylim([0 2]);
        set(gca,'YTick', [0:1:2])
        set(gca,'YTickLabel',{'', 'on','setup'})
        
        
        lgd6=legend('CHP running', 'CHP setup');
                set(lgd6, 'Location', 'Northwest', 'Orientation','horizontal');
        xlabel('time');
        ylabel('binary')
        
        
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/6_CHP')   
        
        %% W?rme CHP
        hold off;
        figure(7)
        %subplot(Nplots,2,6);
        hold all;
        grid on;
        name={};
        maxY=0;
        stairs(res.Qd_CHP_T, '-');  
        stairs(res.Qd_HTR_T, '-');
        stairs(res.Qd_THP_T, '-');
        stairs(res.Qd_delta_T, '-');
  
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        legend('P^{CHP}_{thermal}', 'P^{HTR}_{thermal}', 'P^{THP}_{thermal}', 'P^{Curtail}_{thermal}', );
        xlabel('time');
        ylabel('in kW')
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/7_CHP2')  
        
        %% W?rme TESS
        hold off;
        figure(8)
        %subplot(Nplots,2,8);
        hold all;
        grid on
        yyaxis left
        stairs(res.Qd_TESS_T, '-');
        xlabel('time');
        ylabel('in kW')
        
        yyaxis right
         plot(res.Q_TESS_T_1, '-');  
        ylabel('in kWh');
        ylim([0 par.TESS.Q_max]);
        lgd8=legend('P^{TESS}_{thermal}', 'E^{TESS}_{thermal}');
                set(lgd8, 'Location', 'Northwest', 'Orientation','horizontal');
        
        
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/8_TESS')  
        
        %% W?rme Production
        hold off;
        figure(9)
        %subplot(Nplots,2,10);
        hold all;
        grid on
        name={};
        maxY=0;
        prod=res.Qd_CHP_T-res.Qd_TESS_T+res.Qd_HTR_T-res.Qd_delta_T+res.Qd_THP_T;;
        cons=I.QDemand_t;
        y=[prod';cons'];
        bar(y')
        xlim([0 length(y)]);
        
        lgd9=legend('Production^{total}_{thermal}', 'Consumption^{total}_{thermal}');
                set(lgd9, 'Location', 'Northwest', 'Orientation','horizontal');
        xlabel('time');
        ylabel('in kWh')
        
        xlim([0 24*par.T_total]);
        set(gca,'XTick', [0:12:24*par.T_total])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 8 2],'PaperPositionMode','manual','PaperSize',[8 2])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/9_Totalw')  
        
        
        %% ?bersicht
        figure('Position',[20 100 700 700])
        hold off;
        hold all;
        grid on
        %YCOMP=[ KPI.Econs, KPI.PVprod,  KPI.E_CHP, KPI.Gridpur, -KPI.PVcurt, -KPI.Gridsold, KPI.E_ESScrg, KPI.E_ESSdcrg, KPI.E_ESSlost];
        YCOMP=[ KPI.E_ESSlost,  KPI.E_ESSdcrg,  KPI.E_ESScrg, -KPI.Gridsold, -KPI.PVcurt, KPI.Gridpur,  KPI.E_CHP,  KPI.PVprod, KPI.Econs];
        x=[1,2,3,4,5,6,7,8,9];
        ax2=barh(YCOMP);
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);

        set(gca,'YTickLabel',{'','P_ESS^{losses}', 'P_ESS^{discharge}','P_ESS^{charge}', 'Grid^{supply}','PV^{curt}', 'Grid^{demand}', 'CHP^{prod}','PV^{prod}', 'Demand'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
        set(gcf,'Units','centimeters','PaperPosition',[0 0 5 8],'PaperPositionMode','manual','PaperSize',[5 8])
        print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/Simulation/10_EnergyTotalperiod') 
end