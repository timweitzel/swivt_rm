function [parvar]=ParameterVariation(sel, par)
if sel==1
    parvar(1).ID=1;
    parvar(1).name='ESSprice';
    parvar(1).vect=[100 200 300 500];
    parvar(1).n_max=numel(parvar(1).vect);

    parvar(2).ID=2;
    parvar(2).name='ESSsize';
    parvar(2).vect=[50 100 150 200];
    parvar(2).n_max=numel(parvar(2).vect);

    parvar(3).ID=3;
    parvar(3).name='TESSsize_mult';
    parvar(3).vect=[1, 2];
    parvar(3).n_max=numel(parvar(3).vect);

    parvar(4).ID=4;
    parvar(4).name='CHPsize';
    parvar(4).vect=[20; 50; 70; 140];%[20; 50; 70; 140]; %(variant, Pmax)
    parvar(4).n_max=length(parvar(4).vect);

    parvar(4).CHP(1).alpha=[9999; 2.720; 2.107; 1.975];
    parvar(4).CHP(1).eta=[0.00001; 0.699; 0.613; 0.613];    
    parvar(4).CHP(2).alpha=[9999; 1.840; 1.684; 1.620];
    parvar(4).CHP(2).eta=[0.00001; 0.535; 0.542; 0.559];    
    parvar(4).CHP(3).alpha=[9999; 1.886; 1.604; 1.643];
    parvar(4).CHP(3).eta=[0.00001; 0.541; 0.535; 0.564];
    parvar(4).CHP(4).alpha=[9999; 1.857; 1.629; 1.479];
    parvar(4).CHP(4).eta=[0.00001; 0.573; 0.55; 0.539];

    parvar(5).ID=5;
    parvar(5).name='Delta_NetworkCharges';
    parvar(5).vect=[par.Price.NC_delta, 125, par.Price.total_addon_grid];
    parvar(5).n_max=numel(parvar(5).vect);

    parvar(6).ID=6;
    parvar(6).name='PV Size';
    parvar(6).vect=[1];%[0.75 1 1.25];
    parvar(6).n_max=numel(parvar(6).vect);

    parvar(7).ID=7;
    parvar(7).name='BESS Aging';
    parvar(7).vect=[0.75 1 1.25];
    parvar(7).n_max=numel(parvar(7).vect);
else % price Var
     parvar(1).ID=1;
    parvar(1).name='ESSprice';
    parvar(1).vect=[100 200 300 500];
    parvar(1).n_max=numel(parvar(1).vect);
    
    parvar(2).ID=2;
    parvar(2).name='ESSsize';
    parvar(2).vect=[50 100 150 200];
    parvar(2).n_max=numel(parvar(2).vect);
    
    parvar(3).ID=3;
    parvar(3).name='TESSsize_mult';
    parvar(3).vect=[1];%[1, 2];
    parvar(3).n_max=numel(parvar(3).vect);
    
    parvar(4).ID=4;
    parvar(4).name='CHPsize';
    parvar(4).vect=[50];%[20; 50; 70; 140]; %(variant, Pmax)
    parvar(4).n_max=length(parvar(4).vect);
    
%     parvar(4).CHP(1).alpha=[9999; 2.720; 2.107; 1.975];
%     parvar(4).CHP(1).eta=[0.00001; 0.699; 0.613; 0.613];    
    parvar(4).CHP(1).alpha=[9999; 1.840; 1.684; 1.620];
    parvar(4).CHP(1).eta=[0.00001; 0.535; 0.542; 0.559];    
%     parvar(4).CHP(3).alpha=[9999; 1.886; 1.604; 1.643];
%     parvar(4).CHP(3).eta=[0.00001; 0.541; 0.535; 0.564];
%     parvar(4).CHP(4).alpha=[9999; 1.857; 1.629; 1.479];
%     parvar(4).CHP(4).eta=[0.00001; 0.573; 0.55; 0.539];
    
    parvar(5).ID=5;
    parvar(5).name='Delta_NetworkCharges';
    parvar(5).vect=[par.Price.NC_delta, 125, par.Price.total_addon_grid];
    parvar(5).n_max=numel(parvar(5).vect);
    
    parvar(6).ID=6;
    parvar(6).name='PV Size';
    parvar(6).vect=[1];%[0.75 1 1.25];
    parvar(6).n_max=numel(parvar(6).vect);
    
    parvar(7).ID=7;
    parvar(7).name='BESS Aging';
    parvar(7).vect=[1 0.75 1.25];
    parvar(7).n_max=numel(parvar(7).vect);
    
end
end