clear all
handles=findall(0,'type','figure');
handles.delete;


SOC=[0:0.01:1];
T=25;
n_SOC=length(SOC);


damage=zeros(n_SOC,1);
damage_lin=zeros(n_SOC,1);

ul=1;
delta_t=60;
C_verl=0.2;

%% Linarized model

SOCA=[0:0.1:1.00];
n_SA=length(SOCA);
option=1;

for nsoc=1:n_SOC
    for nsa=1:n_SA-1
        if SOC(nsoc)>= SOCA(nsa) && SOC(nsoc)< SOCA(nsa+1)
            w=(SOC(nsoc)- SOCA(nsa+1))/(SOCA(nsa)-SOCA(nsa+1));
            damage(nsoc,1)          = w * BA_cal_Sarasketa_v1(SOCA(nsa),T,delta_t,C_verl,15) + (1-w) * BA_cal_Sarasketa_v1(SOCA(nsa+1),T,delta_t,C_verl,15);
            damage_lin(nsoc,1)      = BA_cal_Sarasketa_v1(SOC(nsoc),T,delta_t,C_verl,15);
        end
         if SOC(nsoc)== SOCA(n_SA)
            damage(nsoc,1)          = BA_cal_Sarasketa_v1(SOCA(n_SA),T,delta_t,C_verl,15);
            damage_lin(nsoc,1)      = BA_cal_Sarasketa_v1(SOC(nsoc),T,delta_t,C_verl,15);
        end
    end
end


%% Delta calculation

delta=damage-damage_lin;
num=0;
E=0;
SE=0;
max_d=damage(1,1);
min_d=damage(1,1);
for nsoc=1:n_SOC
           E=E+delta(nsoc,1);
           SE=SE+delta(nsoc,1)^2;
           num=num+1;
           max_d=max(max_d, damage(nsoc,1));
           min_d=min(min_d, damage(nsoc,1));
end
RMSE=sqrt(SE/num);
NRMSE=RMSE/(max_d-min_d);
ME=E/num;



%% plot
figure(1)


name={};
symbol={'^', 'v', '>', '<', 'd', 's', 'x', '*', 'o', '+'};

hold on;
grid on;

plot(SOC*100,damage*1000);
plot(SOC*100,damage_lin*1000,'--');

xl2=xlabel('SOC [in %]');
xlim([0 100]);
yl2=ylabel(strcat('\epsilon_{cal} [in ',char(8240),']'));
yl2.FontSize=12;
xl2.FontSize=12;
ylim([0 0.02]);
lgd2=legend('model', 'linearized' );
set(lgd2, 'Location', 'Northwest')

set(gcf,'Units','centimeters','PaperPosition',[0 0 8 5],'PaperPositionMode','manual','PaperSize',[8 5])
print(gcf,'-r300','-djpeg', '-opengl','/Users/Timm/Dropbox/Promotion/BA_cal_linearization')