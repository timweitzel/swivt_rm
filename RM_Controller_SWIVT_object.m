handles=findall(0,'type','figure');
handles.delete;
clear all


%% Options for optimization
opt.INCL_LCCinf             =1; %=1 falls LCC in Zielfunktion aktiv geschaltet werden sollen
opt.INCL_LCC                =1;
opt.INCL_CACinf             =1; %=1 falls CAC in Zielfunktion aufgenommen werden sollen

opt.cplex.DisplayIter       =0;% 1-Iteration werden angezeigt, 0- werden nicht angezeigt %
opt.cplex.TimeCap           =2*60;%5*60min TimeCap
opt.cplex.SolTolerance      =0.005;%0.005;
opt.INCL_epsconstraint      =0;
opt.Multiple_Periods        =0; %Multiple period analysis

% Parameter Variation - only select one option!
opt.INCL_parvar_ESSprice    =0;
opt.INCL_parvar_LCConoff    =0;
opt.INCL_parvar_CAConoff    =0;
opt.INCL_parvar_LCCCAConoff =0;
opt.INCL_parvar_Scenarios   =1;

opt.n_workers               =1; %parfor activated for n_workers>1

opt.save_conc_res           =1; %save sub results
opt.save_cplex              =0; %1: Cplex objekte werden gespeichert

opt.INCL_THP        = 0; %on/off switchfor THP
opt.THP_COP_var     = 0; % 1-THP varoable COP/ 0-electric Heater or THP with constant COP

%% Create Class
SSUP=Simulation_SuperClass_RM(2, opt);
SSUP=SSUP.update_folder(2);


SSUP=SSUP.InititateSimulation;
SSUP=SSUP.StartSimulation;


