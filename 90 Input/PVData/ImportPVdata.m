
function [PV_t] = ImportPVdata(delta_T,day,month,year,stretch)
% this function imports price data from data base and transfers it to an
% array with time stepsize delta_T. Delta_T is measured in minutes.

%clear all

file=strcat(pwd,'\PVData\PV.mat');
load(file)

% year=2015;
% month=8;
% day=1;
% delta_T=5;
% stretch=1;

name=strcat('PV');

matrix=table2array(eval(name));
date=year*10000+month*100+day;

[v,index]=max(matrix(:,4)==date);

PV_1=matrix(index,5:end);

N=60/delta_T;

%step
% for i=1:24
%     for n=1:N
%        PV_t((i-1)*N+n)=PV_1(i)*stretch;
%     end
% end

%linearized
for i=1:24
    for n=1:N
        if i<24
            PV_t((i-1)*N+n)=(PV_1(i)*(N-(n-1))/N+PV_1(i+1)*(n-1)/N)*stretch;
        else
            PV_t((i-1)*N+n)=(PV_1(i)*(N-(n-1))/N+0*(n-1)/N)*stretch;
        end
    end
end

end