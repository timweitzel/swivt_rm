function [p_DAM_t] = ImportPriceDatacube(delta_T, Tag, Monat, Jahr, duration, stretch)
% this function imports price data from data base and transfers it to an
% array with time stepsize delta_T. Delta_T is measured in minutes.
% Duration in day to import multiple days

%clear all
file=strcat(pwd,'/90 Input/PriceData/SpotPrices.mat');
load(file)

for d=1:duration
    t=datetime(Jahr,Monat,Tag)+(d-1);
    date=year(t)*10000+month(t)*100+day(t);
    name=strcat('SpotPrices', num2str(year(t)));
    matrix=table2array(eval(name));
    [v,index]=max(matrix(:,1)==date);
    p_DAM1=matrix(index,2:end);
    N=60/delta_T;
    for i=1:24
        for n=1:N
           p_DAM_t((d-1)*24*N +(i-1)*N+n)=p_DAM1(i)*stretch;
        end
    end
end
end