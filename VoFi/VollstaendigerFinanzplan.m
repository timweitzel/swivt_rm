handles=findall(0,'type','figure');
handles.delete;
clear all

%Das Ganze Teil erzeugt Ordner f�r VoFi-Ergebnisse
    Ordner_VoFi=pwd; %Aktueller Ordner
    cd(Ordner_VoFi);
    Ordner1=strcat(pwd,'/');
    cd(Ordner1);
    Ordnername=strcat('VoFi Ergebnisse_', datestr(now,'yy-mm-dd'));
    mkdir(Ordnername);
    folder=strcat(Ordner1,Ordnername);
    cd(Ordner_VoFi);
    clearvars Ordner1;


%Einlesen der Simulationsergbenisse aus RM_Controller
    Excelname.outputdata = 'outputdata'; %Name der Excel-Ergebnisse der Simulation 
    SimErg = xlsread(Excelname.outputdata); %Output aus Simulation einlesen


%Einlesen der Parameter aus Excel und Definition der Variablen
    Excelname.Parameter = 'Vofi_Parametereinstellungen'; %Name der Excel mit Parametern
    VoFiPar = xlsread(Excelname.Parameter); %Parameter einlesen
    Parameter.Zinsen_Anlage = VoFiPar(1,1);
    Parameter.Zinsen_FK = VoFiPar(2,1);
    Parameter.EK_Quote = VoFiPar(3,1);
    Parameter.HebelsatzGewSt = VoFiPar(4,1);
    Parameter.Laufzeit_Tilgung = VoFiPar(5,1);
    Parameter.Dachflaeche_PV = VoFiPar(6,1); %Dachfl�che in m� f�r Photovoltaik 
    Parameter.Dachflaeche_ST = VoFiPar(7,1); %Dachfl�che in m� f�r Solarthermie
    Parameter.Dachflaeche_DZ = VoFiPar(8,1); %Dachfl�che in m� f�r Autarq Dachsteine
    Parameter.Jahresmiete_Bauverein = VoFiPar(9,1);%Miete im Jahr, die an den Bauverein gezahlt wird
    %Berechnung fester Werte aus Parametern
    Parameter.FK_Tilgungssatz  = (((1+Parameter.Zinsen_FK)^Parameter.Laufzeit_Tilgung)*Parameter.Zinsen_FK)/(((1+Parameter.Zinsen_FK)^Parameter.Laufzeit_Tilgung)-1); %Bererchnung des Prozentsatzes der j�hrlichen Tilgung (Abh�ngig von FK-Zins und Laufzeit)

    Anzahl_PV = size(SimErg); %Anzahl der PV aus Ergebnisverktor RM

%Einlesen der Kostendatei
Excelname.Kostenvektor = 'Kostenvektor';
Kost.Matrix = xlsread(Excelname.Kostenvektor); %Einlesen der Kosten

%VoFi Aufbau


    %�bersichtsvektordefinition �ber alle PV
    VoFiUeb.S1 = (Anzahl_PV:1);
    VoFiUeb.S2 = (Anzahl_PV:1);
    VoFiUeb.S3 = (Anzahl_PV:1);
    VoFiUeb.S4 = (Anzahl_PV:1);
    VoFiUeb.S5 = (Anzahl_PV:1);


%Schleife der VoFis f�r alle PV 
for SimErgZeile=1:1:Anzahl_PV(1,1) %Zeile 1 bis n aller Parametervariationen wird berechnet

%VoFi wird berechnet
    Batteriepreis_ID = SimErg(SimErgZeile, 3); %Gibt ESS-Preis ID wieder
    Batteriekapazitaet_ID = SimErg(SimErgZeile,5); %Gibt BAtteriekapazit�t-ID f�r Parametervariation wieder
    ThermischerSpeicher_ID = SimErg(SimErgZeile, 7); %ID Thermischer Speicher
    BHKW_ID = SimErg(SimErgZeile,9); %Gibt BHKW-ID f�r Parametervariation wieder
    Kost.AK.BHKW = Kost.Matrix(1,1);
%VoFi Aufbau    
VoFiSpalte.S1 = {'Zeitplan' 'Operative Betriebst�tigkeit' '' '' '' '' '' 'Eigenkapital' '+' '-' '+' 'Fremdkapital' '+' '-' '-' 'Standardanlage' '-' '+' '+' 'Steuerzahlast' '-' '-' '-' 'Finanzierungssaldo' '' '' 'Bestandsgr��en' '-' '+' 'Bestandssaldo'}';
VoFiSpalte.S2 = {'' '' '' '' '' '' '' '' 'Anfangsbestand' 'Entnahme' 'Einlage' '' 'Aufnahme' 'Sollzins' 'Tilgiung' '' 'Anlage' 'Aufl�sung' 'Habenszins' '' 'KSt' 'Soli' 'GewSt' '' '' '' '' 'Fremdkapitalstand' 'Guthabensaldo' ''}';%
VoFiSpalte.S3 = {2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}';
VoFiSpalte.S4 = {Batteriepreis_ID, Batteriekapazitaet_ID, ThermischerSpeicher_ID, BHKW_ID,1,1,1,1,1,1,1,1,2,3,4,5,6,7,8,9,1,2,5,6,8,7,1,3,1,1}';

%Erzeugung des VoFi
VoFi = [VoFiSpalte.S1 VoFiSpalte.S2 VoFiSpalte.S3 VoFiSpalte.S4];

% Ausgabe als Excel
    cd(Ordnername);
    Text1=string(SimErg(SimErgZeile, 1));
    Text2=string(BHKW_ID);
    Excel.name1 = strcat('VoFi PV_ID',Text1,',',' BHKW_ID ',Text2);
    xlswrite(Excel.name1, VoFi);
    cd(Ordner_VoFi);
    clearvars Text1 Text2;

%Vektor f�r Ergebniss�bersicht Schreiben
VoFiUeb.S1(SimErgZeile,1)=1; %PV ID
VoFiUeb.S2(SimErgZeile,1)=2;
VoFiUeb.S3(SimErgZeile,1)=3;
VoFiUeb.S4(SimErgZeile,1)=4;
VoFiUeb.S5(SimErgZeile,1)=5;

end

%�bersichtsstruktur erzeugen
Hilfsmatrix.h2 = [VoFiUeb.S1 VoFiUeb.S2 VoFiUeb.S3 VoFiUeb.S4 VoFiUeb.S5];
Kopfzeile.z2 = {'PV_ID', 'Investitionssumme','EK', 'Endwert', 'Zins'};
Uebersichtmatrix = [Kopfzeile.z2; num2cell(Hilfsmatrix.h2)];
    % Ausgabe als Excel
    cd(Ordnername);
    Excel.name2 = strcat('VoFi PV_ID �bersicht');
    xlswrite(Excel.name2, Uebersichtmatrix);
    cd(Ordner_VoFi);
    

%L�schen Hilfsvariablen
clearvars Ordnername Ordner_VoFi Kopfzeile Excelname Hilfsmatrix VoFiSpalte VoFiUeb



