classdef Simulation_SuperClass_RM
    %SIMULATION_SUPERCLASS_RM controls Simulation_RM
    %   Detailed explanation goes here
    
    properties
            Sim, ... % Type simulation for every simulation
            par, ... % parameter for base. will partially be overwritten by parvar
            opt, ... % options for base
            In, ... % input for base
            PVdata,...
            PriceData_p,...
            PriceData_s,...
            PDemandData,...
            QDemandData,...
            parvar,... %parameters for parameter variation
            folder_store,... %foler string for save
            folder_store_details,...
            folder_sim, ... %base folder with simulation files
            folder_graphs, ... %base folder with simulation files
            Sol,...
            Savename
    end
    
    methods
        
        
        function obj = update_folder(obj,sel)
            folder=Folderlocations(sel);
            obj.folder_store =  folder.store;
            obj.folder_store_details= strcat(folder.store,'/Simulationsergebnisse_', datestr(now,'yy-mm-dd'));
            mkdir(obj.folder_store_details);
            obj.folder_sim   =  folder.sim;
            obj.folder_graphs=  folder.graphs;
            obj.Savename=strcat('Simulation', datestr(now,'yymmdd'));
        end
        
        function obj = Simulation_SuperClass_RM(parvar_select, opt)
            Parameterset;
            obj.par         =par;
            obj.parvar      =ParameterVariation(parvar_select, par);
            
            % Load PVdata PriceData PDemandData QDemandData
            T_O=obj.par.T_O;
            for d=1: obj.par.T_total
                l_datetime=obj.par.sdate.datetime+(d-1);
                obj.PriceData_p(:,d)     =   obj.par.Price.pmult        * ImportPriceDatacube(   obj.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  T_O, 1)';
                obj.PriceData_s(:,d)     =   obj.PriceData_p(:,d);%obj.par.Price.Grid_Sell    * ones(size(obj.PriceData_p(:,d)));
                for pv=1:obj.parvar(6).n_max
                    obj.PVdata(:,d,pv)          = ImportPVdata_v2(      obj.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  T_O, obj.parvar(6).vect(pv))'; %Stretch is set to 1
                end
                obj.PDemandData(:,d)     = ImportPDemanddata_v2( obj.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  T_O)';
                obj.QDemandData(:,d)     = ImportQDemanddata_v2( obj.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  T_O)';
            end
            
            obj.opt        =opt;
            %obj.In         =In;
            obj.Savename    =strcat('Sim_',num2str(yyyymmdd(datetime('now'))));
        end 
        
        function [pv, pv_max] = calculatePV(obj, p1v, p2v, p3v, p4v, p5v, p6v, p7v)
            p7max=obj.parvar(7).n_max;
            p67max=p7max*obj.parvar(6).n_max;
            p57max=p67max*obj.parvar(5).n_max;
            p47max=p57max*obj.parvar(4).n_max;
            p37max=p47max*obj.parvar(3).n_max;
            p27max=p37max*obj.parvar(2).n_max;
            p17max=p27max*obj.parvar(1).n_max;
            
            pv=(p1v-1)*p27max + (p2v-1) * p37max + (p3v-1)*p47max +(p4v-1)*p57max + (p5v-1)*p67max + (p6v-1)*p7max + p7v;
            pv_max=p17max;
        end
            
        function obj = InititateSimulation(obj)
           for p1v=1:obj.parvar(1).n_max
               for p2v=1:obj.parvar(2).n_max
                    for p3v=1:obj.parvar(3).n_max
                        for p4v=1:obj.parvar(4).n_max
                            for p5v=1:obj.parvar(5).n_max
                                for p6v=1:obj.parvar(6).n_max
                                    for p7v=1:obj.parvar(7).n_max
                                        
                                        [pv, pv_max]= obj.calculatePV(p1v, p2v, p3v, p4v, p5v, p6v, p7v);
                                        
                                        obj.Sol.Sim(pv,1)=Simulation_RM(pv, obj.par, obj.opt, obj.folder_store_details, obj.folder_sim, pv_max);
                                        
                                        obj.Sol.Sim(pv,1).par.ESS.E           =  obj.parvar(2).vect(p2v);
                                        obj.Sol.Sim(pv,1).par.ESS.E_max       =  obj.parvar(2).vect(p2v);
                                        obj.Sol.Sim(pv,1).par.ESS.P_c_max     =  obj.parvar(2).vect(p2v)*3;
                                        obj.Sol.Sim(pv,1).par.ESS.P_d_max     =  obj.parvar(2).vect(p2v)*3;
                                        if obj.parvar(2).vect(p2v)==0
                                            obj.Sol.Sim(pv,1).opt.INCL_LCCinf             =0;
                                            obj.Sol.Sim(pv,1).opt.INCL_CACinf             =0;
                                            obj.Sol.Sim(pv,1).opt.INCL_LCC                =0;
                                        end
                                        
                                        obj.Sol.Sim(pv,1).par.ESS.price       =  obj.parvar(1).vect(p1v);
                                        obj.Sol.Sim(pv,1).par.ESS.E_inv       =  obj.parvar(1).vect(p1v) * obj.parvar(2).vect(p2v);
                                        
                                        obj.Sol.Sim(pv,1).par.CHP.P           =  obj.parvar(4).vect(p4v,1);
                                        obj.Sol.Sim(pv,1).par.CHP.P_max       =  obj.parvar(4).vect(p4v,1);
                                        obj.Sol.Sim(pv,1).par.CHP.P_min       =  obj.parvar(4).vect(p4v,1)*0.5;
                                        obj.Sol.Sim(pv,1).par.CHP.alpha       =   obj.parvar(4).CHP(p4v).alpha;
                                        obj.Sol.Sim(pv,1).par.CHP.eta         =   obj.parvar(4).CHP(p4v).eta;
                                        
                                        obj.Sol.Sim(pv,1).par.TESS.Q_max      = obj.parvar(4).vect(p4v,1)* obj.parvar(4).CHP(p4v).alpha(length(obj.par.CHPmodes))*obj.parvar(3).vect(p3v) * 50 * 1.163 * 30/1000;
                                        
                                        obj.Sol.Sim(pv,1).par.Price.pmult     =  1;
                                        
                                        obj.Sol.Sim(pv,1).par.Price.total_addon_micro= obj.par.Price.total_addon_grid - obj.parvar(5).vect(p5v);
                                        obj.Sol.Sim(pv,1).par.ESS.LCC.cal_SOC     = obj.par.ESS.LCC.cal_SOC * obj.parvar(7).vect(p7v);
                                        obj.Sol.Sim(pv,1).par.ESS.LCC.cyc_DOD     = obj.par.ESS.LCC.cyc_DOD * obj.parvar(7).vect(p7v);
                                         

                                        % Sol.pv as parvar Verzeichnis
                                        obj.Sol.pv(pv).ID=pv;
                                        obj.Sol.pv(pv).ESSprice     =obj.parvar(1).vect(p1v);
                                        obj.Sol.pv(pv).ESSpriceID   =p1v;
                                        obj.Sol.pv(pv).ESSsize      =obj.parvar(2).vect(p2v);
                                        obj.Sol.pv(pv).ESSsizeID    =p2v;
                                        obj.Sol.pv(pv).TESSsize     =obj.parvar(3).vect(p3v);
                                        obj.Sol.pv(pv).TESSsizeID   =p3v;
                                        obj.Sol.pv(pv).CHPsize      =obj.parvar(4).vect(p4v);
                                        obj.Sol.pv(pv).CHPsizeID    =p4v;
                                        obj.Sol.pv(pv).NWC          =obj.parvar(5).vect(p5v);
                                        obj.Sol.pv(pv).NWCID        =p5v;
                                        obj.Sol.pv(pv).pmult        =obj.parvar(6).vect(p6v);
                                        obj.Sol.pv(pv).pmultID      =p6v;
                                        obj.Sol.pv(pv).BACmult      =obj.parvar(7).vect(p7v);
                                        obj.Sol.pv(pv).BACID        =p7v;
                                        
                                        obj.Sol.par=obj.par;
                                        obj.Sol.opt=obj.opt;
                                        
                                    end
                                end
                            end
                        end
                    end
               end
           end
           obj.Sim=obj.Sol.Sim;
        end
        
        function obj = StartSimulation(obj)
            tges=datetime('now');
            for p1v=1:obj.parvar(1).n_max
               for p2v=1:obj.parvar(2).n_max
                    for p3v=1:obj.parvar(3).n_max
                        for p4v=1:obj.parvar(4).n_max
                            for p5v=1:obj.parvar(5).n_max
                                for p6v=1:obj.parvar(6).n_max
                                    for p7v=1:obj.parvar(7).n_max
                                        [pv, pv_max]=obj.calculatePV(p1v, p2v, p3v, p4v, p5v, p6v, p7v);
                                        obj.Sim(pv)=obj.Sim(pv).solve(obj.PVdata(:,:,p6v), obj.PDemandData, obj.QDemandData, obj.PriceData_p, obj.PriceData_s, pv_max, tges);
                                        
                                        obj.Sol.Sim(pv).ID=pv;
                                        obj.Sol.Sim(pv).par=obj.Sim(pv).par;
                                        obj.Sol.Sim(pv).KPI=obj.Sim(pv).KPI;
                                        obj.Sol.Sim(pv).opt=obj.Sim(pv).opt;

                                        obj.Sol.Sim(pv).KPI.Tot_BatteryLifetimePrediction         =   obj.Sim(pv).par.ESS.E_inv/obj.Sim(pv).KPI.OF_Cost_LCC *obj.Sim(pv).par.T_total/365;
                                        obj.Sol.Sim(pv).KPI.Tot_BatteryAvgCycDay                  =   obj.Sim(pv).KPI.E_ESScrg *obj.Sim(pv).par.ESS.eta_c / obj.Sim(pv).par.ESS.E /obj.Sim(pv).par.T_total;
                                        obj.Sol.Sim(pv).KPI.Tot_BatteryAvgSOC_p100                =    sum( obj.Sim(pv).res.E_ESSstore_T_1) / (obj.Sim(pv).par.T_total*24*obj.par.delta_T/60) /obj.Sim(pv).par.ESS.E*100;    
                                        obj.Sol.Sim(pv).KPI.Tot_Profit                  =   -obj.Sim(pv).KPI.OF_Cost;
                                        obj.Sol.Sim(pv).KPI.Tot_GridPurchase_p100       =    obj.Sim(pv).KPI.E_Gridpur/(obj.Sim(pv).KPI.E_cons + obj.Sim(pv).KPI.E_ESSlost)*100;
                                        obj.Sol.Sim(pv).KPI.Tot_GridSold_p100           =    obj.Sim(pv).KPI.E_Gridsold/(obj.Sim(pv).KPI.E_CHP + obj.Sim(pv).KPI.E_PVprod)*100;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP_heatshare_p100      =    obj.Sim(pv).KPI.H_CHP/(obj.Sim(pv).KPI.H_cons + obj.Sim(pv).KPI.H_TESScrg + obj.Sim(pv).KPI.H_TESSdcrg)*100;
                                        obj.Sol.Sim(pv).KPI.Tot_THP_heatshare_p100      =    obj.Sim(pv).KPI.H_THP/(obj.Sim(pv).KPI.H_cons + obj.Sim(pv).KPI.H_TESScrg + obj.Sim(pv).KPI.H_TESSdcrg)*100;
                                        obj.Sol.Sim(pv).KPI.Tot_HTR_heatshare_p100      =    obj.Sim(pv).KPI.H_HTR/(obj.Sim(pv).KPI.H_cons + obj.Sim(pv).KPI.H_TESScrg + obj.Sim(pv).KPI.H_TESSdcrg)*100;
                                        obj.Sol.Sim(pv).KPI.Tot_TESSAvgCycDay           =    (obj.Sim(pv).KPI.H_TESScrg - obj.Sim(pv).KPI.H_TESSdcrg) /2 / obj.Sim(pv).par.TESS.Q_max /obj.Sim(pv).par.T_total;
                                        obj.Sol.Sim(pv).KPI.Tot_TESSAvgSOC_p100         =    sum( obj.Sim(pv).res.Q_TESS_T_1) / (obj.Sim(pv).par.T_total*24*obj.par.delta_T/60) /obj.Sim(pv).par.TESS.Q_max*100;    
                                        obj.Sol.Sim(pv).KPI.Tot_CHP_AvgP_p100           =    obj.Sim(pv).KPI.E_CHP/obj.Sim(pv).KPI.h_CHP/obj.Sim(pv).par.CHP.P*100;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP_h_to_sup          	=    obj.Sim(pv).KPI.h_CHP/obj.Sim(pv).KPI.s_CHP;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP_h                   =    obj.Sim(pv).KPI.h_CHP;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP2BESS_p100           =   obj.Sim(pv).KPI.E_CHP2BESS/obj.Sim(pv).KPI.E_CHP*100;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP2grid_p100           =   obj.Sim(pv).KPI.E_CHP2grid/obj.Sim(pv).KPI.E_CHP*100;
                                        obj.Sol.Sim(pv).KPI.Tot_CHP2dem_p100            =   obj.Sim(pv).KPI.E_CHP2dem/obj.Sim(pv).KPI.E_CHP*100;
                                        obj.Sol.Sim(pv).KPI.Tot_PV2BESS_p100            =   obj.Sim(pv).KPI.E_PV2BESS/obj.Sim(pv).KPI.E_PVprod*100;
                                        obj.Sol.Sim(pv).KPI.Tot_PV2dem_p100             =   obj.Sim(pv).KPI.E_PV2dem/obj.Sim(pv).KPI.E_PVprod*100;
                                        obj.Sol.Sim(pv).KPI.Tot_PV2grid_p100            =   obj.Sim(pv).KPI.E_PV2grid/obj.Sim(pv).KPI.E_PVprod*100;
                                        obj.Sol.Sim(pv).TIME.t_sim_start                =    obj.Sim(pv).TIME.t_sim_start;
                                        obj.Sol.Sim(pv).TIME.t_sim_end                  =    obj.Sim(pv).TIME.t_sim_end;
                                        obj.Sol.Sim(pv).KPI.t_sim_delta                 =    obj.Sim(pv).TIME.t_sim_end-obj.Sim(pv).TIME.t_sim_start;
                                        
                                        obj.Sim(pv).res=[];
                                        try
                                            obj.save
                                        catch
                                        end
                                        writexlsx(obj.Sol, obj.folder_store, pv);
                                        cd(obj.folder_sim);
                                    end
                                end
                            end
                        end
                    end
               end 
            end
            obj.save
        end
                 
        
        function obj =save(obj)
        %Function saves Obj. into predefined folder
            cd(obj.folder_store);
            eval(strcat(obj.Savename,'=obj;'));
            save(strcat(obj.Savename,'.mat'), strcat(obj.Savename),'-v7.3');
            cd(obj.folder_sim);   
        end
        
                
    end
    
end

