classdef Simulation_RM
    %SIMULATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
            ID, ...
            name,...
            res,...
            TIME,...
            mipgap,...
            status,...
            solution,...
            par,...
            I,...
            opt,...
            KPI,...
            Time,...
            perday,...
            folder_store,... %foler string for save
            folder_sim, ... %base folder with simulation files
            folder_graphs, ... %base folder with simulation files
    end
    
    methods
        function obj=Simulation_RM(ID, par, opt, folder_store, folder_sim, pv_max )
            obj.ID=ID;
            obj.par=par;
            obj.opt=opt;
            obj.folder_store=folder_store;
            obj.folder_sim=folder_sim;
            obj.name=strcat('Sim','_', num2str(ID), '_', num2str(pv_max));
        end
        
        function obj=solve(obj, PVdata, PDemandData, QDemandData, PriceData_p, PriceData_s,  pv_max, tges)

            global par opt perday
            par=obj.par;
            opt=obj.opt;
            pv=obj.ID;
            perday=[];
            for d=1:par.T_total

                perday(d).t_sim.start=datetime('now');
                if d==1
                    obj.TIME.t_sim_start=perday(d).t_sim.start;
                end
                % Parameter?bergabe in jede Tagesoptimierung
                perday(d).par = par;
                
                if par.T_O_Master>par.T_total-d+1 %Anpassung der Optimierungszeitr?ume, so dass am Ender der Laufzeit nur noch ein Tag optimiret wird
                    perday(d).par.T_O     = par.T_total-d+1;
                    perday(d).par.T       = 24/(par.delta_T/60)*perday(d).par.T_O; %Optimierungszeitraum in Zeiteinheiten delta_T
                    perday(d).par.T_1     = perday(d).par.T + 1;
                end
                
                % Inputdefinition f?r jede Tagesoptimierung
                %%% Time periods investigated
                perday(d).I.datetime    =   perday(d).par.sdate.datetime+(d-1);
                l_datetime              =   perday(d).I.datetime;
                %perday(d).I.eps_const.n =   eps; %Laufparameter 1:num-1
                
                if opt.THP_COP_var ==1
                    perday(d).par.THP.COP= COPperDay(day(l_datetime ),    month(l_datetime ), year(l_datetime ));
                end
                
                %%% energy prices, heat data, pv Production
                perday(d).I.pd_t          = PriceData_p(1:perday(d).par.T_O*24,  d);
                perday(d).I.ps_t          = PriceData_s(1:perday(d).par.T_O*24,  d);
                perday(d).I.PV_t          = PVdata(1:perday(d).par.T_O*24, d);
                perday(d).I.PDemand_t     = PDemandData(1:perday(d).par.T_O*24, d);
                perday(d).I.QDemand_t     = QDemandData(1:perday(d).par.T_O*24, d);
                perday(d).I.Pres_t        = perday(d).I.PDemand_t   -   perday(d).I.PV_t;
                perday(d).I.Pres_t_pos    = perday(d).I.Pres_t(1:par.T_C);
                perday(d).I.Pres_t_pos    = perday(d).I.Pres_t_pos(perday(d).I.Pres_t_pos>0);
                clearvars l_datetime
                
                %Anfangs- und Endbedingungen
                if d==1
                    perday(d).I.start.Q_TESS_T0      = 0.0 *  par.TESS.Q_max;
                    perday(d).I.start.E_ESS_T0       = 0.0 *  par.ESS.E_max; %Anfangswert und gleichzeitig Endwert einer Betrachtung
                    perday(d).I.start.S_run_CHP_T0   =0;
                    perday(d).I.start.S_run_CHP_T0_num=0; %Anzahl Perioden die das BHKW schon gelaufen ist
                    
                    perday(d).I.end.Q_TESS_T0        = 0.0 *  par.TESS.Q_max;
                    perday(d).I.end.E_ESS_T0         = 0.0 *  par.ESS.E_max;
                    
                else
                    perday(d).I.start.Q_TESS_T0      = perday(d-1).end.Q_TESS_T_1;
                    perday(d).I.start.E_ESS_T0       = perday(d-1).end.E_ESSstore_T_1;
                    perday(d).I.start.S_run_CHP_T0   = perday(d-1).end.CHP_run;
                    if perday(d-1).end.CHP_run==1 % indieser Abfrage wird ermittelt wie lange das BHKW in der Vorperiode schon gelaufen ist.
                        perday(d).I.start.S_run_CHP_T0_num   = 0;
                        l=length(perday(d-1).res.S_run_CHP_T);
                        l_check=perday(d-1).res.S_run_CHP_T(l-1);
                        while  l_check==1 && l>1
                            perday(d).I.start.S_run_CHP_T0_num=perday(d).I.start.S_run_CHP_T0_num+1;
                            l=l-1;
                            if l>1
                                l_check=perday(d-1).res.S_run_CHP_T(l-1);
                            else
                                l_check=0;
                            end
                        end
                    else
                        perday(d).I.start.S_run_CHP_T0_num   = 0;
                    end
                    
                    
                    perday(d).I.end.Q_TESS_T0         = par.TESS.Q_max  *0.0;
                    perday(d).I.end.E_ESS_T0          = par.ESS.E_max   *0.0;
                    
                end
                
                % Kontrollhorizont
                perday(d).I_TC.datetime       =  perday(d).I.datetime;
                %perday(d).I_TC.eps_const      =  perday(d).I.eps_const;
                perday(d).I_TC.pd_t           =  perday(d).I.pd_t(1:par.T_C);
                perday(d).I_TC.ps_t           =  perday(d).I.pd_t(1:par.T_C);
                perday(d).I_TC.PV_t           =  perday(d).I.PV_t(1:par.T_C);
                perday(d).I_TC.PDemand_t      =  perday(d).I.PDemand_t(1:par.T_C);
                perday(d).I_TC.QDemand_t      =  perday(d).I.QDemand_t(1:par.T_C);
                perday(d).I_TC.Pres_t         =  perday(d).I.Pres_t(1:par.T_C);
                
                %% Optimierung
                
                [perday(d).res  perday(d).end    perday(d).x_var    perday(d).cplex  perday(d).opt] = RM_OM (  perday(d).par,      perday(d).I,    opt);
                perday(d).end.CHP_run=perday(d).res.S_run_CHP_T(perday(d).par.T_C); %Endstatus des CHP runs wird mit?bernommen
                
                perday(d).t_sim.end      = datetime('now');
                obj.TIME.t_sim_end       = perday(d).t_sim.end;
                perday(d).t_sim.status   = perday(d).cplex.Solution.statusstring;
                sim_t                    = perday(d).t_sim.end-perday(d).t_sim.start;
                sim_t2                   = perday(d).t_sim.end-tges;
                sim_status               = perday(d).t_sim.status;
                sim_gap                  = perday(d).res.mipgap;
                
                %% Zuordnung abspeichern der Ergebnisse
                perday(d).TIME.t_date         = perday(d).I_TC.datetime;
                perday(d).KPI = obj.Calc_dailyKPIs(perday(d).res , perday(d).I_TC, perday(d).I); 
                
                if d==1
                    obj.KPI  = perday(d).KPI;
                    obj.TIME.t_start  = perday(d).I_TC.datetime;
                    obj.res  = perday(d).res;
                    obj.I    = perday(d).I_TC;
                else
                    obj.TIME.t_end  = perday(d).I_TC.datetime;
                    if opt.save_conc_res==1
                        [obj.res, obj.KPI, obj.I]=concatenateResults_RM02(obj.res, obj.KPI, obj.I, perday(d).res, perday(d).KPI, perday(d).I_TC);
                    else
                        [obj.KPI ]=concatenateResults_RM02_reduced( obj.KPI, perday(d).KPI);
                        obj.perday(d).cplex=[];
                    end
                end
                if opt.save_cplex==0
                    Sim_local.perday(d).cplex=[];
                end
                
                S=sprintf(strcat('ParVar: ', num2str(pv),'/',num2str( pv_max),' -- Day: ', num2str(d), '/', num2str(par.T_total), ' -- t_iter: ', datestr(sim_t,'HH:MM:SS'), ' -- t_ges: ', datestr(sim_t2,'dd:HH:MM:SS'),' -- gap: ', num2str( ceil(sim_gap*1000)/10 ), ' %%', ' -- status: ', sim_status));
                
                disp(S);
                
                if opt.save_conc_res==1
                    if d==par.T_total
                        obj.res.E_ESSstore_T_1(par.T_total*24*par.delta_T/60)    =  perday(d).end.E_ESSstore_T_1;
                        obj.res.Q_TESS_T_1(par.T_total*24*par.delta_T/60)        =  perday(d).end.Q_TESS_T_1;
                    end
                end
                
            end
            obj.save;
            
        end
        
        function KPI=Calc_dailyKPIs(obj, res, I_TC, I)  
            dt=obj.par.delta_T/60;
            KPI.E_cons         = sum( I_TC.PDemand_t )            *dt;
            KPI.E_PVprod       = sum( I_TC.PV_t)                  *dt;
            KPI.E_PVcurt       = sum( res.P_Curt_PV_T)            *dt;
            KPI.E_PV2grid       = sum( res.P_PV_g_T)            *dt;
            KPI.E_PV2BESS       = sum( res.P_PV_s_T)            *dt;
            KPI.E_PV2dem       = sum( res.P_PV_d_T)            *dt;
            KPI.E_CHP         = sum( res.P_CHP_T)                *dt;
            KPI.E_CHP2grid         = sum( res.P_CHP_g_T)                *dt;
            KPI.E_CHP2BESS         = sum( res.P_CHP_s_T)                *dt;
            KPI.E_CHP2dem         = sum( res.P_CHP_d_T)                *dt;
            KPI.E_THP         = sum( res.P_THP_T)                *dt;
            KPI.E_Gridpur       = sum( res.P_Grid_d_T)             *dt;
            KPI.E_Gridsold      = sum( res.P_Grid_s_T)             *dt;
            KPI.E_ESScrg      = sum( res.P_ESScrg_T)             *dt;
            KPI.E_ESSdcrg     = sum( res.P_ESSdcrg_T)            *dt;
            KPI.E_ESSlost     = sum( res.E_ESSlosses_T)          *dt;
            
            KPI.h_CHP           = sum( res.S_run_CHP_T)             *dt;
            KPI.s_CHP           = sum( res.S_sup_CHP_T)           ;
            %KPI.ESS_SOCmin      = res.D_SOCMIN;
            %KPI.ESS_SOCmax      = res.D_SOCMAX;
            %KPI.ESS_DOD         = res.D_DODper;
            KPI.H_cons          = sum( I_TC.QDemand_t )            *dt;
            KPI.H_CHP           = sum( res.Qd_CHP_T)                *dt;
            KPI.H_THP           = sum( res.Qd_THP_T)                *dt;
            KPI.H_HTR           = sum( res.Qd_HTR_T)                *dt;
            KPI.H_TESScrg       = sum( res.Qd_TESS_T(res.Qd_TESS_T>0))                *dt;
            KPI.H_TESSdcrg      = sum( res.Qd_TESS_T(res.Qd_TESS_T<0))                *dt;
            %KPI.H_curt          = sum( res.Qd_delta_T)                *dt;
            
            KPI.OF_Cost_Grid_d     = sum( res.C_Grid_d_T);
            KPI.OF_Cost_Grid_s     = sum( res.C_Grid_s_T);
            KPI.OF_Cost_Cust       = sum( res.C_Cust_T);
            KPI.OF_Cost_Custheat   = sum( res.C_Custheat_T);
            KPI.OF_Cost_Prod       = sum( res.C_Prod_T);
            KPI.OF_Cost_CHP_run    = sum( res.C_run_CHP_T);
            KPI.OF_Cost_CHP_sup    = sum( res.C_sup_CHP_T);
            KPI.OF_Cost_HTR        = sum( res.C_HTR_T);
            KPI.OF_Cost_CYC        = sum( res.C_CYC);
            KPI.OF_Cost_CAC        = sum( res.C_CAC_T);
            KPI.OF_Cost_LCC        = KPI.OF_Cost_CAC + KPI.OF_Cost_CYC;
            
            KPI.OF_Cost            = KPI.OF_Cost_Cust +KPI.OF_Cost_Custheat + KPI.OF_Cost_Prod + KPI.OF_Cost_Grid_d + KPI.OF_Cost_Grid_s + KPI.OF_Cost_CHP_run + KPI.OF_Cost_CHP_sup + KPI.OF_Cost_HTR + KPI.OF_Cost_LCC;
            
            KPI.OF_Aut_z          = sum( res.P_Grid_d_T)          *dt;
            KPI.OF_Aut_n          = sum( I.Pres_t_pos)            *dt;
            KPI.OF_Autarchy       = (1-    KPI.OF_Aut_z  /   KPI.OF_Aut_n )    *   100;
            
            KPI.Tot_CHP2BESS_p100           =   KPI.E_CHP2BESS/KPI.E_CHP*100;
            KPI.Tot_CHP2grid_p100           =   KPI.E_CHP2grid/KPI.E_CHP*100;
            KPI.Tot_CHP2dem_p100            =   KPI.E_CHP2dem/KPI.E_CHP*100;
            KPI.Tot_PV2BESS_p100            =   KPI.E_PV2BESS/KPI.E_PVprod*100;
            KPI.Tot_PV2dem_p100            =   KPI.E_PV2dem/KPI.E_PVprod*100;
            KPI.Tot_PV2grid_p100            =   KPI.E_PV2grid/KPI.E_PVprod*100;
            
        end
       
        function obj=save(obj)
            cd(obj.folder_store);
            eval(sprintf(strcat(obj.name,' =obj;')));
            save(strcat(obj.name,'.mat'),obj.name);
            cd(obj.folder_sim);
        end
        
    end
    
end

