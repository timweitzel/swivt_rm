classdef Simulation
    %SIMULATION Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
            ID, ...
            name,...
            F,...
            x_res,...
            x_var,...
            time,...
            mipgap,...
            status,...
            solution,...
            par,...
            In,...
            opt,...
            delta_E1,...
            delta_E2,...
            fl_int,...
            BESS_lifeloss,...
            BESS_MaxAge_ina,...
    end
    
    methods
        function obj=Simulation(ID, par, In, opt, name)
            obj.ID=ID;
            obj.par=par;
            obj.opt=opt;
            obj.In=In;
            if nargin<5
                obj.name='';
            else
                obj.name=name;
            end
        end
        
        function obj=solve(obj, PROBLEM, x_var)
            if nargin <2 
                [PROBLEM, x_var]=   FFSP_OM(obj.par, obj.In, obj.opt); % In case of flexcase Cplex Problem is created externally
            end
            [obj.x_res, obj.x_var]=     FFSP_OMSolverSub(obj.par, obj.In, obj.opt, x_var, PROBLEM);
            obj.F           =obj.x_res.I_CT;
            obj.time        =obj.x_res.time;
            obj.mipgap      =obj.x_res.mipgap;
            obj.status      =obj.x_res.status;
            obj.solution    =obj.x_res.solution;
        end
        
        function obj=plot_Results(obj)
            Machineresultplots(obj.returnSim);
        end
        
        function obj=excelexport_machineplan(obj)
            excelexport_machineplan (obj.returnSim);
        end
        
        function obj=plot_Schedule(obj, printname, path)
            if nargin <=1
               s_print=0; %0 entspricht not print
               printname='test'; 
               path='';
            else
                s_print=1;
            end
            plotMachineplan_production(obj.returnSim, s_print, printname, path );
        end
        
        function Sim=returnSim(obj) %old format used in subfunctions
            Sim=obj.x_res;
            Sim.par=obj.par;
            Sim.opt=obj.opt;
            Sim.In=obj.In;
        end
        
        function obj=calc_BESSAging(obj)
            SOC=obj.x_res.E_ESSstore_T_1/obj.par.ESS.E_max*100;
            delta_t_ins=obj.par.delta_T*60;
            [obj.BESS_lifeloss] =AlterungskurveBESS(SOC, delta_t_ins)*100; %Lifeloss in %
            obj.BESS_MaxAge_ina=1/obj.BESS_lifeloss*100/365; %Maximale Anzahl an Jahren
        end
            
    end
    
end

